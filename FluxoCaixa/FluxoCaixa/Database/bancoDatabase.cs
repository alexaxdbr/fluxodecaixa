﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluxoCaixa.Database
{
    class bancoDatabase
    {
        Models.db_fluxocaixaEntities db = new Models.db_fluxocaixaEntities();

        public void CadastrarBanco (Models.tb_banco banco)
        {
            db.tb_banco.Add(banco);
            db.SaveChanges();
        }

        public List<Models.tb_banco> ListarTodos()
        {
            List<Models.tb_banco> lista = db.tb_banco.OrderBy(t => t.nm_banco)
                                                     .ToList();
            return lista;
        }

        public void RemoverBanco(int id)
        {
            Models.tb_banco remover = db.tb_banco.FirstOrDefault(t => t.idtb_banco == id);
            db.tb_banco.Remove(remover);
            db.SaveChanges();
        }


    }
}
