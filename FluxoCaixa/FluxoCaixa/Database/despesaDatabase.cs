﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluxoCaixa.Database
{
    class despesaDatabase
    {
        Models.db_fluxocaixaEntities db = new Models.db_fluxocaixaEntities();

        public void CadastrarDespesa(Models.tb_despesa despesa)
        {
            db.tb_despesa.Add(despesa);
            db.SaveChanges();    
        }

        public List<Models.tb_despesa> ListarTodos()
        {
            List<Models.tb_despesa> lista = db.tb_despesa.ToList();
            return lista;
        }

        public void AlterarDespesa(Models.tb_despesa despesa)
        {
            Models.tb_despesa alterar = db.tb_despesa.FirstOrDefault(t => t.idtb_despesa
                                                                == despesa.idtb_despesa);
            if(alterar != null)
            {
                alterar.dt_pagamento = despesa.dt_pagamento;
                alterar.nm_despesa = despesa.nm_despesa;
                alterar.tb_banco_idtb_banco = despesa.tb_banco_idtb_banco;
                alterar.vl_pago = despesa.vl_pago;
                alterar.vl_saldoTotal = despesa.vl_saldoTotal;                                   
            }

            db.SaveChanges();
        }

        public void RemoverDespesa(int id)
        {
            Models.tb_despesa remover = db.tb_despesa.First(t => t.idtb_despesa == id);
            db.tb_despesa.Remove(remover);
            db.SaveChanges();
        }
    }
}
