﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluxoCaixa.Database;
using FluxoCaixa.Database.Models;

namespace FluxoCaixa.Business
{
    class bancoBusiness
    {
        bancoDatabase db = new bancoDatabase();

        public void CadastrarBanco(tb_banco banco)
        {
            db.CadastrarBanco(banco);
        }

        public List<tb_banco> ListaTodos()
        {
            List<tb_banco> lista = db.ListarTodos();
            return lista;
        }

        public void RemoverBanco(int id)
        {
            db.RemoverBanco(id);
        }
    }
}
