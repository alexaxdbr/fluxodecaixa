﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluxoCaixa.Database.Models;

namespace FluxoCaixa.Business
{
    class despesaBusiness        
    {
        Database.despesaDatabase db = new Database.despesaDatabase();

        public void CadastrarDespesa(tb_despesa despesa)
        {
            db.CadastrarDespesa(despesa);
        }

        public List<tb_despesa> ListarTodos()
        {
            List<tb_despesa> lista = db.ListarTodos();
            return lista;
        }

        public void AlterarDespesa(tb_despesa despesa)
        {
            db.AlterarDespesa(despesa);
        }
        
        public void RemoverDespesa(int id)
        {
            db.RemoverDespesa(id);
        }
    }
}
